#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>

#include <lodepng.h>

#include <omp.h>
#include <mpi.h>
#include <vector>
#include <cstring>

#define TAG_REQUEST_TASK 0
#define TAG_DISPATCH_TASK 1
#define TAG_SEND_RESULT 2

#define GLM_FORCE_SWIZZLE  // vec3.xyz(), vec3.xyx() ...ect, these are called "Swizzle".
// https://glm.g-truc.net/0.9.1/api/a00002.html
//
#include <glm/glm.hpp>
// for the usage of glm functions
// please refer to the document: http://glm.g-truc.net/0.9.9/api/a00143.html
// or you can search on google with typing "glsl xxx"
// xxx is function name (eg. glsl clamp, glsl smoothstep)

#define pi 3.1415926535897932384626433832795

struct ThreadVars{
    int j;
    int m;
    int n;
};

typedef glm::dvec2 vec2;  // doube precision 2D vector (x, y) or (u, v)
typedef glm::dvec3 vec3;  // 3D vector (x, y, z) or (r, g, b)
typedef glm::dvec4 vec4;  // 4D vector (x, y, z, w)
typedef glm::dmat3 mat3;  // 3x3 matrix

unsigned int num_threads;  // number of thread
unsigned int width;        // image width
unsigned int height;       // image height
vec2 iResolution;          // just for convenience of calculation

int AA = 2;  // anti-aliasing

double power = 8.0;           // the power of the mandelbulb equation
double md_iter = 24;          // the iteration count of the mandelbulb
double ray_step = 10000;      // maximum step of ray marching
double shadow_step = 1500;    // maximum step of shadow casting
double step_limiter = 0.2;    // the limit of each step length
double ray_multiplier = 0.1;  // prevent over-shooting, lower value for higher quality
double bailout = 2.0;         // escape radius
double eps = 0.0005;          // precision
double FOV = 1.5;             // fov ~66deg
double far_plane = 100.;      // scene depth

vec3 camera_pos;  // camera position in 3D space (x, y, z)
vec3 target_pos;  // target position in 3D space (x, y, z)

unsigned char* raw_image;  // 1D image
unsigned char** image;     // 2D image

// save raw_image to PNG file
void write_png(const char* filename) {
    unsigned error = lodepng_encode32_file(filename, raw_image, width, height);

    if (error) printf("png error %u: %s\n", error, lodepng_error_text(error));
}

// mandelbulb distance function (DE)
// v = v^8 + c
// p: current position
// trap: for orbit trap coloring : https://en.wikipedia.org/wiki/Orbit_trap
// return: minimum distance to the mandelbulb surface
double md(vec3 p, double& trap) {
    vec3 v = p;
    double dr = 1.;             // |v'|
    double r = glm::length(v);  // r = |v| = sqrt(x^2 + y^2 + z^2)
    trap = r;

    for (int i = 0; i < md_iter; ++i) {
        double theta = glm::atan(v.y, v.x) * power;
        double phi = glm::asin(v.z / r) * power;
        dr = power * glm::pow(r, power - 1.) * dr + 1.;
        v = p + glm::pow(r, power) *
                    vec3(cos(theta) * cos(phi), cos(phi) * sin(theta), -sin(phi));  // update vk+1

        // orbit trap for coloring
        trap = glm::min(trap, r);

        r = glm::length(v);      // update r
        if (r > bailout) break;  // if escaped
    }
    return 0.5 * log(r) * r / dr;  // mandelbulb's DE function
}

// scene mapping
double map(vec3 p, double& trap, int& ID) {
    vec2 rt = vec2(cos(pi / 2.), sin(pi / 2.));
    vec3 rp = mat3(1., 0., 0., 0., rt.x, -rt.y, 0., rt.y, rt.x) *
              p;  // rotation matrix, rotate 90 deg (pi/2) along the X-axis
    ID = 1;
    return md(rp, trap);
}

// dummy function
// becase we dont need to know the ordit trap or the object ID when we are calculating the surface
// normal
double map(vec3 p) {
    double dmy;  // dummy
    int dmy2;    // dummy2
    return map(p, dmy, dmy2);
}

// simple palette function (borrowed from Inigo Quilez)
// see: https://www.shadertoy.com/view/ll2GD3
vec3 pal(double t, vec3 a, vec3 b, vec3 c, vec3 d) {
    return a + b * glm::cos(2. * pi * (c * t + d));
}

// second march: cast shadow
// also borrowed from Inigo Quilez
// see: http://www.iquilezles.org/www/articles/rmshadows/rmshadows.htm
double softshadow(vec3 ro, vec3 rd, double k) {
    double res = 1.0;
    double t = 0.;  // total distance
    for (int i = 0; i < shadow_step; ++i) {
        double h = map(ro + rd * t);
        res = glm::min(res, k * h / t);  // closer to the objects, k*h/t terms will produce darker shadow
        if (res < 0.02) return 0.02;
        t += glm::clamp(h, .001, step_limiter);  // move ray
    }
    return glm::clamp(res, .02, 1.);
}

// use gradient to calc surface normal
vec3 calcNor(vec3 p) {
    vec2 e = vec2(eps, 0.);
    return normalize(vec3(map(p + e.xyy()) - map(p - e.xyy()),  // dx
        map(p + e.yxy()) - map(p - e.yxy()),                    // dy
        map(p + e.yyx()) - map(p - e.yyx())                     // dz
        ));
}

// first march: find object's surface
double trace(vec3 ro, vec3 rd, double& trap, int& ID) {
    double t = 0;    // total distance
    double len = 0;  // current distance

    for (int i = 0; i < ray_step; ++i) {
        len = map(ro + rd * t, trap, ID);  // get minimum distance from current ray position to the object's surface
        if (glm::abs(len) < eps || t > far_plane) break;
        t += len * ray_multiplier;
    }
    return t < far_plane
               ? t
               : -1.;  // if exceeds the far plane then return -1 which means the ray missed a shot
}





struct MyArg_dispatch{
    int* i_ptr;
    int world_size;
};

void* dispatch_task(void* arg){
    int* i_ptr = ((MyArg_dispatch*)arg)->i_ptr;
    int world_size = ((MyArg_dispatch*)arg)->world_size;
    int i;

    MPI_Status status;
    while (*i_ptr < height){
        MPI_Recv(&i, 1, MPI_INT, MPI_ANY_SOURCE, TAG_REQUEST_TASK, MPI_COMM_WORLD, &status);
        i = (*i_ptr)++;

        MPI_Send(&i, 1, MPI_INT, status.MPI_SOURCE, TAG_DISPATCH_TASK, MPI_COMM_WORLD);
    }
    
    // for (int cnt=0; cnt < world_size-1; cnt++){ // inform that all tasks have been dispatched; we don't need to inform rank 0
    for (int cnt=0; cnt < world_size; cnt++){ // inform that all tasks have been dispatched; we don't need to inform rank 0
        MPI_Recv(&i, 1, MPI_INT, MPI_ANY_SOURCE, TAG_REQUEST_TASK, MPI_COMM_WORLD, &status);
        i = -1;
        MPI_Send(&i, 1, MPI_INT, status.MPI_SOURCE, TAG_DISPATCH_TASK, MPI_COMM_WORLD);
    }

    return 0;
}

void* result_receiver(void* total_pixel_p){
    int total_pixel = (int)*((double*)total_pixel_p);
     
    for (int num_done_pixel = 0; num_done_pixel < total_pixel; num_done_pixel++){
        
        unsigned char recvBuf[12];
        MPI_Status status;
        int processingRow;
        int j;

        MPI_Recv(recvBuf, 12, MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, TAG_SEND_RESULT, MPI_COMM_WORLD, &status);
        processingRow = *((int*)recvBuf);
        j = *((int*)(recvBuf+4));
        image[processingRow][4 * j + 0] = recvBuf[8]; // r
        image[processingRow][4 * j + 1] = recvBuf[9]; // g
        image[processingRow][4 * j + 2] = recvBuf[10]; // b
        image[processingRow][4 * j + 3] = recvBuf[11]; // a
    }
    return 0;
}



int main(int argc, char** argv) {

    // ./source [num_threads] [x1] [y1] [z1] [x2] [y2] [z2] [width] [height] [filename]
    // num_threads: number of threads per process
    // x1 y1 z1: camera position in 3D space
    // x2 y2 z2: target position in 3D space
    // width height: image size
    // filename: filename
    assert(argc == 11);

    //---init arguments
    num_threads = atoi(argv[1]);
    camera_pos = vec3(atof(argv[2]), atof(argv[3]), atof(argv[4]));
    target_pos = vec3(atof(argv[5]), atof(argv[6]), atof(argv[7]));
    width = atoi(argv[8]);
    height = atoi(argv[9]);

    double total_pixel = width * height;

    iResolution = vec2(width, height);
    //---

    //---create image
    raw_image = new unsigned char[width * height * 4];
    image = new unsigned char*[height];

    for (int i = 0; i < height; ++i) {
        image[i] = raw_image + i * width * 4;
    }
    //---





    int rank, world_size;
    // MPI_Init(&argc, &argv);
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided < MPI_THREAD_MULTIPLE)
    {
       printf("Error: the MPI library doesn't provide the required thread level\n");
       MPI_Abort(MPI_COMM_WORLD, 0);
    }

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);



    //---rendering
    //
    // MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0){

        // printf("rank,%d,thread,%d,time,%lf,start_computing.\n", rank, omp_get_thread_num(), MPI_Wtime());
        int i = 0;

        pthread_t dispatchTaskTid;
        pthread_t resultReceiverTid;
        MyArg_dispatch myarg;
        myarg = (MyArg_dispatch){.i_ptr = &i, .world_size = world_size};
        pthread_create(&dispatchTaskTid, NULL, dispatch_task, &myarg);
        pthread_create(&resultReceiverTid, NULL, result_receiver, (void*)&total_pixel);


        { // do the task the same as rank != 0

            std::vector<ThreadVars> threadVarsVec;
            for (int j = 0; j < width; ++j){
                for (int m = 0; m < AA; ++m){
                    for (int n = 0; n < AA; ++n){
                        threadVarsVec.push_back((ThreadVars){.j=j, .m=m, .n=n});
                    }
                }
            }

            // printf("rank,%d,thread,%d,time,%lf,end_computing.\n", rank, omp_get_thread_num(), MPI_Wtime());
            int i; 
            MPI_Status status;
            MPI_Send(&i, 1, MPI_INT, 0, TAG_REQUEST_TASK, MPI_COMM_WORLD);
            MPI_Recv(&i, 1, MPI_INT, 0, TAG_DISPATCH_TASK, MPI_COMM_WORLD, &status);

            // printf("rank%d recv task: row %d\n", rank, i);
            while (i != -1){

                vec4 fcol_initVal(0.);
                std::pair<vec4,int> fcolVecInitVal = {fcol_initVal, 0};
                std::vector<std::pair<vec4,int>> fcolVec(width, fcolVecInitVal); // final color of a row (RGBA 0 ~ 1)

                // anti aliasing (a row)
                #pragma omp parallel for schedule(dynamic)
                for (int threadVIdx=0; threadVIdx < threadVarsVec.size(); threadVIdx++) {
                    // printf("rank,%d,thread,%d,time,%lf,start_computing.\n", rank, omp_get_thread_num(), MPI_Wtime());
                    int j = threadVarsVec[threadVIdx].j;
                    int m = threadVarsVec[threadVIdx].m;
                    int n = threadVarsVec[threadVIdx].n;
                    vec2 p = vec2(j, i) + vec2(m, n) / (double)AA;

                    //---convert screen space coordinate to (-ap~ap, -1~1)
                    // ap = aspect ratio = width/height
                    vec2 uv = (-iResolution.xy() + 2. * p) / iResolution.y;
                    uv.y *= -1;  // flip upside down
                    //---

                    //---create camera
                    vec3 ro = camera_pos;               // ray (camera) origin
                    vec3 ta = target_pos;               // target position
                    vec3 cf = glm::normalize(ta - ro);  // forward vector
                    vec3 cs =
                        glm::normalize(glm::cross(cf, vec3(0., 1., 0.)));  // right (side) vector
                    vec3 cu = glm::normalize(glm::cross(cs, cf));          // up vector
                    vec3 rd = glm::normalize(uv.x * cs + uv.y * cu + FOV * cf);  // ray direction
                    //---

                    //---marching
                    double trap;  // orbit trap
                    int objID;    // the object id intersected with
                    double d = trace(ro, rd, trap, objID);
                    //---

                    //---lighting
                    vec3 col(0.);                          // color
                    vec3 sd = glm::normalize(camera_pos);  // sun direction (directional light)
                    vec3 sc = vec3(1., .9, .717);          // light color
                    //---

                    //---coloring
                    if (d < 0.) {        // miss (hit sky)
                        col = vec3(0.);  // sky color (black)
                    } else {
                        vec3 pos = ro + rd * d;              // hit position
                        vec3 nr = calcNor(pos);              // get surface normal
                        vec3 hal = glm::normalize(sd - rd);  // blinn-phong lighting model (vector
                                                             // h)
                        // for more info:
                        // https://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_shading_model

                        // use orbit trap to get the color
                        col = pal(trap - .4, vec3(.5), vec3(.5), vec3(1.),
                            vec3(.0, .1, .2));  // diffuse color
                        vec3 ambc = vec3(0.3);  // ambient color
                        double gloss = 32.;     // specular gloss

                        // simple blinn phong lighting model
                        double amb =
                            (0.7 + 0.3 * nr.y) *
                            (0.2 + 0.8 * glm::clamp(0.05 * log(trap), 0.0, 1.0));  // self occlution
                        double sdw = softshadow(pos + .001 * nr, sd, 16.);         // shadow
                        double dif = glm::clamp(glm::dot(sd, nr), 0., 1.) * sdw;   // diffuse
                        double spe = glm::pow(glm::clamp(glm::dot(nr, hal), 0., 1.), gloss) *
                                     dif;  // self shadow

                        vec3 lin(0.);
                        lin += ambc * (.05 + .95 * amb);  // ambient color * ambient
                        lin += sc * dif * 0.8;            // diffuse * light color * light intensity
                        col *= lin;

                        col = glm::pow(col, vec3(.7, .9, 1.));  // fake SSS (subsurface scattering)
                        col += spe * 0.8;                       // specular
                    }
                    //---

                    col = glm::clamp(glm::pow(col, vec3(.4545)), 0., 1.);  // gamma correction

                    #pragma omp critical
                    {
                        fcolVec[j].first += vec4(col, 1.);
                        fcolVec[j].second++;

                        if (fcolVec[j].second == AA*AA){

                            vec4 fcol = fcolVec[j].first / (double)(AA * AA);
                            // convert double (0~1) to unsigned char (0~255)
                            fcol *= 255.0;

                            unsigned char sendBuf[12]={0};
                            std::memcpy(sendBuf+0, (int*)&i, 4);
                            std::memcpy(sendBuf+4, (int*)&j, 4);
                            sendBuf[8] = fcol.r;
                            sendBuf[9] = fcol.g;
                            sendBuf[10] = fcol.b;
                            sendBuf[11] = 255;

                            // printf("rank,%d,thread,%d,time,%lf,start_sending_result.\n", rank, omp_get_thread_num(), MPI_Wtime());
                            MPI_Send(sendBuf, 12, MPI_UNSIGNED_CHAR, 0, TAG_SEND_RESULT, MPI_COMM_WORLD);
                            // printf("rank,%d,thread,%d,time,%lf,end_sending_result.\n", rank, omp_get_thread_num(), MPI_Wtime());
                        }
                    }

                    // printf("rank,%d,thread,%d,time,%lf,end_computing.\n", rank, omp_get_thread_num(), MPI_Wtime());
                } // end of anti aliasing (a row)

                // request a new task
                // printf("rank%d done task: row %d\n", rank, i);
                MPI_Send(&i, 1, MPI_INT, 0, TAG_REQUEST_TASK, MPI_COMM_WORLD);
                MPI_Recv(&i, 1, MPI_INT, 0, TAG_DISPATCH_TASK, MPI_COMM_WORLD, &status);
                // printf("rank%d recv task: row %d\n", rank, i);
            } // end of while (i != -1)


        } // end of the task the same as rank != 0

        pthread_join(dispatchTaskTid, NULL);
        pthread_join(resultReceiverTid, NULL);
    } // end of if rank == 0
    else { // rank != 0

        // printf("rank,%d,thread,%d,time,%lf,start_computing.\n", rank, omp_get_thread_num(), MPI_Wtime());
        std::vector<ThreadVars> threadVarsVec;
        for (int j = 0; j < width; ++j){
            for (int m = 0; m < AA; ++m){
                for (int n = 0; n < AA; ++n){
                    threadVarsVec.push_back((ThreadVars){.j=j, .m=m, .n=n});
                }
            }
        }

        // printf("rank,%d,thread,%d,time,%lf,end_computing.\n", rank, omp_get_thread_num(), MPI_Wtime());
        int i; 
        MPI_Status status;
        MPI_Send(&i, 1, MPI_INT, 0, TAG_REQUEST_TASK, MPI_COMM_WORLD);
        MPI_Recv(&i, 1, MPI_INT, 0, TAG_DISPATCH_TASK, MPI_COMM_WORLD, &status);

        // printf("rank%d recv task: row %d\n", rank, i);
        while (i != -1){

            vec4 fcol_initVal(0.);
            std::pair<vec4,int> fcolVecInitVal = {fcol_initVal, 0};
            std::vector<std::pair<vec4,int>> fcolVec(width, fcolVecInitVal); // final color of a row (RGBA 0 ~ 1)

            // anti aliasing (a row)
            #pragma omp parallel for schedule(dynamic)
            for (int threadVIdx=0; threadVIdx < threadVarsVec.size(); threadVIdx++) {
                // printf("rank,%d,thread,%d,time,%lf,start_computing.\n", rank, omp_get_thread_num(), MPI_Wtime());
                int j = threadVarsVec[threadVIdx].j;
                int m = threadVarsVec[threadVIdx].m;
                int n = threadVarsVec[threadVIdx].n;
                vec2 p = vec2(j, i) + vec2(m, n) / (double)AA;

                //---convert screen space coordinate to (-ap~ap, -1~1)
                // ap = aspect ratio = width/height
                vec2 uv = (-iResolution.xy() + 2. * p) / iResolution.y;
                uv.y *= -1;  // flip upside down
                //---

                //---create camera
                vec3 ro = camera_pos;               // ray (camera) origin
                vec3 ta = target_pos;               // target position
                vec3 cf = glm::normalize(ta - ro);  // forward vector
                vec3 cs =
                    glm::normalize(glm::cross(cf, vec3(0., 1., 0.)));  // right (side) vector
                vec3 cu = glm::normalize(glm::cross(cs, cf));          // up vector
                vec3 rd = glm::normalize(uv.x * cs + uv.y * cu + FOV * cf);  // ray direction
                //---

                //---marching
                double trap;  // orbit trap
                int objID;    // the object id intersected with
                double d = trace(ro, rd, trap, objID);
                //---

                //---lighting
                vec3 col(0.);                          // color
                vec3 sd = glm::normalize(camera_pos);  // sun direction (directional light)
                vec3 sc = vec3(1., .9, .717);          // light color
                //---

                //---coloring
                if (d < 0.) {        // miss (hit sky)
                    col = vec3(0.);  // sky color (black)
                } else {
                    vec3 pos = ro + rd * d;              // hit position
                    vec3 nr = calcNor(pos);              // get surface normal
                    vec3 hal = glm::normalize(sd - rd);  // blinn-phong lighting model (vector
                                                         // h)
                    // for more info:
                    // https://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_shading_model

                    // use orbit trap to get the color
                    col = pal(trap - .4, vec3(.5), vec3(.5), vec3(1.),
                        vec3(.0, .1, .2));  // diffuse color
                    vec3 ambc = vec3(0.3);  // ambient color
                    double gloss = 32.;     // specular gloss

                    // simple blinn phong lighting model
                    double amb =
                        (0.7 + 0.3 * nr.y) *
                        (0.2 + 0.8 * glm::clamp(0.05 * log(trap), 0.0, 1.0));  // self occlution
                    double sdw = softshadow(pos + .001 * nr, sd, 16.);         // shadow
                    double dif = glm::clamp(glm::dot(sd, nr), 0., 1.) * sdw;   // diffuse
                    double spe = glm::pow(glm::clamp(glm::dot(nr, hal), 0., 1.), gloss) *
                                 dif;  // self shadow

                    vec3 lin(0.);
                    lin += ambc * (.05 + .95 * amb);  // ambient color * ambient
                    lin += sc * dif * 0.8;            // diffuse * light color * light intensity
                    col *= lin;

                    col = glm::pow(col, vec3(.7, .9, 1.));  // fake SSS (subsurface scattering)
                    col += spe * 0.8;                       // specular
                }
                //---

                col = glm::clamp(glm::pow(col, vec3(.4545)), 0., 1.);  // gamma correction

                #pragma omp critical
                {
                    fcolVec[j].first += vec4(col, 1.);
                    fcolVec[j].second++;

                    if (fcolVec[j].second == AA*AA){

                        vec4 fcol = fcolVec[j].first / (double)(AA * AA);
                        // convert double (0~1) to unsigned char (0~255)
                        fcol *= 255.0;

                        unsigned char sendBuf[12]={0};
                        std::memcpy(sendBuf+0, (int*)&i, 4);
                        std::memcpy(sendBuf+4, (int*)&j, 4);
                        sendBuf[8] = fcol.r;
                        sendBuf[9] = fcol.g;
                        sendBuf[10] = fcol.b;
                        sendBuf[11] = 255;

                        // printf("rank,%d,thread,%d,time,%lf,start_sending_result.\n", rank, omp_get_thread_num(), MPI_Wtime());
                        MPI_Send(sendBuf, 12, MPI_UNSIGNED_CHAR, 0, TAG_SEND_RESULT, MPI_COMM_WORLD);
                        // printf("rank,%d,thread,%d,time,%lf,end_sending_result.\n", rank, omp_get_thread_num(), MPI_Wtime());
                    }
                }
                // printf("rank,%d,thread,%d,time,%lf,end_computing.\n", rank, omp_get_thread_num(), MPI_Wtime());
            } // end of anti aliasing (a row)

            // request a new task
            // printf("rank%d done task: row %d\n", rank, i);
            MPI_Send(&i, 1, MPI_INT, 0, TAG_REQUEST_TASK, MPI_COMM_WORLD);
            MPI_Recv(&i, 1, MPI_INT, 0, TAG_DISPATCH_TASK, MPI_COMM_WORLD, &status);
            // printf("rank%d recv task: row %d\n", rank, i);
        } // end of while (i != -1)


    } // end of if (rank != 0)
    //---


    // printf("rank%d idles.\n", rank);
    
    // //---saving image
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0){
        write_png(argv[10]);
    }
    //---


    //---finalize
    delete[] raw_image;
    delete[] image;
    //---

    MPI_Finalize();


    return 0;
}
