# Parallel computing: Mandelbulb visualization with **MPI** and **OpenMP**


## Mandelbulb

> "The Mandelbulb is a three-dimensional fractal, constructed for the first time in 1997 by Jules Ruis and in 2009 further developed by Daniel White and Paul Nylander using spherical coordinates." - Wikipedia

![img](./testcases/03.png)


## Goal 

- Mandelbulb Visualization
    - Generate 3D images by ray tracing
    - We use ray marching algorithm
- Given a sequential version of [sample code](./sample/hw2.cc), [parallelize it with MPI and OpenMP](./hw2.cc)
- Understand the importance of load balancing


## Input 

`$ ./hw2 $c $x1 $y1 $z1 $x2 $y2 $z2 $width $height $filename`
- `$c`:	     int,	    Number of thread per process
- `$x1`:	     double,		camera position x
- `$y1`:	     double,		camera position y
- `$z1`:	     double,		camera position z
- `$x2`:	     double,		camera target position x
- `$y2`:	     double,		camera target position y
- `$z2`:	     double,		camera target position z
- `$width`:	unsigned int,	width of the image
- `$height`:	unsigned int,	height of the image
- `$filename`:	string,		file name of the output PNG image


## Output

- Save the results to `$filename`
- The output image should be a 32bit PNG with RGBA channels


## Usage

1. Compile and execute the source code
    ```
    $ make
    $ mpirun -n 3 ./hw2 4 1.1645 2.0475 1.7305 -0.8492 -1.8767 -1.00928 1536 1536 8.png
    ```
    (Launch 3 processes; 4 threads per process)

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
